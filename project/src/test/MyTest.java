package test;
import laboratorna1.FirstLab;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class MyTest {
    @Test
    public void TestForSquareArea(){

        assertEquals(new FirstLab().SquareArea(2),4);
    }
    @Test
    public void TestForWeight(){

        assertEquals(new FirstLab().Weight(2500),2);
    }
    @Test
    public void TestForOdd(){
        assertEquals(new FirstLab().Odd(3),true);
    }
    @Test
    public void TestForFunctionFour(){

        assertEquals(new FirstLab().FunctionFour(4),5);
    }
    @Test
    public void TestForFunctionFive(){

        assertEquals(new FirstLab().FunctionFive(4),"good");
    }
    @Test
    public void TestForFunctionSix(){

        assertEquals(new FirstLab().FunctionSix(1,1),2);
    }
    @Test
    public void TestForFunctionSeven(){

        assertEquals(new FirstLab().FunctionSeven(7,3),2);
    }
    @Test(dataProvider = "arrayProvider")
    public void TestForFunctionEight(int[] array,int l){

        assertEquals(new FirstLab().FunctionEight(array),l);
    }
    @DataProvider
    public Object[][] arrayProvider() {
        return new Object[][]{{new int[]{6, 2, 4, 5, 3, 7, 0, 1, 3, 9},6},
                {new int[]{10, 2, 3, 9},0},
                {new int[]{1, 9, 5, 7},3}

        };
    }

    @Test(dataProvider = "matrixProvider")
    public void TestForFunctionNine(int[][] input,int k1,int k2,int[][] output){
        assertEquals(new FirstLab().FunctionNine(input,k1,k2),output);
    }
    @DataProvider
    public Object[][] matrixProvider(){
        int[][] input = {{2,3,6,9,-9},
                {34,98,-9,2,-1},
                {-4,2,1,5,3},
                {-98,8,4,1,2}};
        int[][] output = {{-9,3,6,9,2},
                {-1,98,-9,2,34},
                {3,2,1,5,-4},
                {2,8,4,1,-98}};
        return new Object[][]{{input,0,4,output}};
    }
}

