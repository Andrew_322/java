package laboratorna1;

public class FirstLab {

    public int SquareArea(int a) {
        return a * a;
    }

    public int Weight(int m) {
        return m / 1000;
    }

    public boolean Odd(int a) {
        if (a % 2 == 1) return true;
        return false;
    }

    public int FunctionFour(int a) {
        if (a > 0) return a + 1;
        return a - 2;
    }

    public String FunctionFive(int k) {
        switch (k) {
            case 1:
                return "bad";
            case 2:
                return "not good";
            case 3:
                return "normal";
            case 4:
                return "good";
            case 5:
                return "exellent";
            default:
                return "Error";
        }
    }

    public double FunctionSix(double x, int n) {
        double s = 1, st = 1, f = 1;
        for (int i = 1; i <= n; i++) {
            st *= x;
            f *= i;
            s += st / f;
        }
        return s;
    }

    public int FunctionSeven(int a, int b) {
        int k = 0, n = 0;
        while (k <= a) {
            k += b;
            n++;
        }
        return n-1;
    }

    public int FunctionEight(int[] a) {
        for (int i = a.length - 2; i > 0; i--) {
            if (a[i] > a[0] && a[i] < a[a.length-1])
                return i+1;
        }
        return 0;
    }

    public int[][] FunctionNine(int[][] matr, int k1, int k2) {
        for (int i = 0; i < matr[0].length; i++) {
            int t = matr[k1][i];
            matr[k1][i] = matr[k2][i];
            matr[k2][i] = t;
        }
        return matr;


    }


}